Messy module, we need a hero!
====================
We need a hero to display a triangle.

We have an incomplete _SimpleFigureTriangle_ class, based on a given number of lines
we should display a triangle.

For 3 lines we should display something like:

    #
    ##
    ###