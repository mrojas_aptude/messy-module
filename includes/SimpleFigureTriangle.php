<?php

/**
 * Class SimpleFigureTriangle
 */
class SimpleFigureTriangle{
  protected $lines;

  /**
   * SimpleFigureTriangle constructor.
   *
   * @param int $lines
   */
  public function __construct($lines = 3) {
    $this->lines = $lines;
  }

  /**
   * Get Triangle body
   *
   * @return string
   */
  public function getTriangle(){
    //Perform implementation
    return '';
  }
}
